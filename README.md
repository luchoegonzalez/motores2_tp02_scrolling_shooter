# Motores Gráficos II - TP02 - Side Scrolling Shooter

Programación para la resolución y modelos 3d hechos por mi.

### Consigna
Desarrolle sobre la demo presentada los siguientes puntos:
- Arreglar ReStart.
- Crear mínimo un nuevo enemigo.
- Agregar mínimo un powerup.
- Bajar velocidad de disparo de enemigos
- Realizarle comentarios al código
- Mejorar Gameplay

Template original: https://github.com/Hjupol/side_scrolling_shooter.git

### Video resolución
[![Watch the video](https://img.youtube.com/vi/86HuoVZVu9Y/hqdefault.jpg)](https://youtu.be/86HuoVZVu9Y)