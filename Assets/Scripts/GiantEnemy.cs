using System.Collections.Generic;
using UnityEngine;

public class GiantEnemy : Controller_Enemy
{
    private Rigidbody rb;

    public static bool enemigoGiganteEnPantalla = false;
    public int vida = 10;
    bool pararMovimiento = false;

    private Transform instanciadorDeBalas1;
    private Transform instanciadorDeBalas2;
    private List<Transform> listaInstanciadoresDeBalas;

    internal override void Start()
    {
        enemigoGiganteEnPantalla = true;
        rb = GetComponent<Rigidbody>();
        transform.rotation = Quaternion.Euler(0, 180, 0);

        instanciadorDeBalas1 = transform.GetChild(6).GetComponent<Transform>();
        instanciadorDeBalas2 = transform.GetChild(7).GetComponent<Transform>();
        listaInstanciadoresDeBalas = new List<Transform> { instanciadorDeBalas1, instanciadorDeBalas2 };

        shootingCooldown = 6.5f;
    }

    // en cada frame le doy movimiento, chequeo los limites y hago que dispare al jugador desde sus dos puntos de disparo
    public override void Update()
    {
        shootingCooldown -= Time.deltaTime;
        Moverse();
        CheckLimits();
        ShootPlayer(listaInstanciadoresDeBalas, 3);
    }

    // muevo al enemigo hacia la izquierda
    private void Moverse()
    {
        if (!pararMovimiento)
        {
            transform.position += new Vector3(-enemySpeed * Time.deltaTime, 0, 0);
        }
    }

    // reutilizo la variable xLimit pero aca en vez de destruir al enemigo hace que pare su movimiento
    private void CheckLimits()
    {
        if (this.transform.position.x <= xLimit)
        {
            pararMovimiento = true;
        }
    }

    // al colisionar va perdiendo vidas en vez de destruirse
    internal override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            Destroy(collision.gameObject);
            PerderVida(1);
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            Destroy(collision.gameObject);
            PerderVida(2);
        }
    }

    void PerderVida(int dano)
    {
        vida -= dano;

        // si la vida llega a 0 se destruye
        if (vida <= 0)
        {
            DestruirNave();
        }
    }

    // si colisiona con el shockwave este enemigo de destruye automaticamente
    internal override void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Explosion"))
        {
            DestruirNave();
        }
    }

    // al destruirse desactiva la variable enemigoGiganteEnPantalla para que se puedan volver a instanciar enemigos
    // ademas suma 5 puntos y genera un powerup
    void DestruirNave()
    {
        enemigoGiganteEnPantalla = false;
        Destroy(this.gameObject);
        Controller_Hud.points += 5;
        GeneratePowerUp();
    }
}
