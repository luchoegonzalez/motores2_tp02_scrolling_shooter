using UnityEngine;

public class WallMovement : MonoBehaviour
{
    public float rapidezMovimiento;

    // agrego movimiento en la pared hacia la izquierda para dar sensacion de movimiento en el jugador
    void Update()
    {
        transform.position = new Vector3(transform.position.x - rapidezMovimiento * Time.deltaTime, transform.position.y, transform.position.z);

        if (transform.position.x < -70)
        {
            transform.position = new Vector3(70, transform.position.y, transform.position.z);
        }
    }
}
