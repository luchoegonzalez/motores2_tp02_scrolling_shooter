﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// clase padre de todos los proyectiles
public class Projectile : MonoBehaviour
{
    public float xLimit = 30;
    public float yLimit = 20;
    
    virtual public void Update()
    {
        CheckLimits();
    }

    //destruyo los proyectiles al colisionar con pared, piso y techo
    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Wall") || collision.gameObject.CompareTag("Floor") || collision.gameObject.CompareTag("Ceiling"))
        {
            Destroy(this.gameObject);
        }
    }

    // chequeo los limites para que no hayan balas fuera de la pantalla, destruyendolas si esto ocurre
    internal virtual void CheckLimits()
    {
        if (this.transform.position.x > xLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.x < -xLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.y > yLimit)
        {
            Destroy(this.gameObject);
        }
        if (this.transform.position.y < -yLimit)
        {
            Destroy(this.gameObject);
        }

    }

}
