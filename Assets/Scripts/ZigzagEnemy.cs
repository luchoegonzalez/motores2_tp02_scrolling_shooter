﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZigzagEnemy : Controller_Enemy
{
    public bool goingUp;

    private Rigidbody rb;

    internal override void Start()
    {
        rb = GetComponent<Rigidbody>();
        base.Start();
    }
    // movimiento que cambia de arriba hacia abajo al tocar el suelo o el techo
    void FixedUpdate()
    {
        if (goingUp)
        {
            rb.AddForce(new Vector3(-1, 1, 0) * enemySpeed);
        }
        else
        {
            rb.AddForce(new Vector3(-1, -1, 0) * enemySpeed);
        }
    }

    internal override void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            goingUp = true;
            //rb.velocity = Vector3.zero;
        }
        if (collision.gameObject.CompareTag("Ceiling"))
        {
            goingUp = false;
            //rb.velocity = Vector3.zero;
        }
        base.OnCollisionEnter(collision);
    }
}
