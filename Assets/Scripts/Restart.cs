﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    // obtengo la posicion inicial del jugador para poder reiniciarla luego
    Vector3 posicionJugadorInicial;
    private void Start()
    {
        posicionJugadorInicial = Controller_Player._Player.gameObject.transform.position;
    }

    void Update()
    {
        GetInput();
    }


    //si aprieto la R vuelvo a cargar la escena reiniciando el nivel, el timeScale, el jugador y los powerups
    private void GetInput()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            GiantEnemy.enemigoGiganteEnPantalla = false;
            Controller_Player._Player.gameObject.SetActive(true);
            Controller_Player._Player.gameObject.transform.position = posicionJugadorInicial;
            Controller_Player._Player.reiniciarPowerUps();
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }
    }
}
