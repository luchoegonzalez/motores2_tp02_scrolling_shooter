using UnityEngine;

public class Parallax : MonoBehaviour
{
    // variables que supongo que querian usar pero descartaron
    private float length, startPos;

    // variables para mover a los objetos
    public float parallaxEffect;

    void Start()
    {
        // seteo las variables
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        // hago que el objeto se mueva hacia la izquierda segun el parallaxEffect que le asigno desde el Inspector
        transform.position = new Vector3(transform.position.x - parallaxEffect * Time.deltaTime, transform.position.y, transform.position.z);

        // si llega a cierto valor se reinicia la posicion
        if (transform.position.x < -length)
        {
            transform.position = new Vector3(length, transform.position.y, transform.position.z);
        }
    }
}
