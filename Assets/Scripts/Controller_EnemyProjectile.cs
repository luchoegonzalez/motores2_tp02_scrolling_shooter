﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//proyectil del enemigo que hereda de la clase Projectile
public class Controller_EnemyProjectile : Projectile
{
    private GameObject player;
    private Vector3 direction;
    private Rigidbody rb;

    public float enemyProjectileSpeed;

    void Start()
    {
        // si hay un jugador seteo la variable player y calculo la direccion hacia donde está
        // para que este proyectil sea disparado hacia alli
        if (Controller_Player._Player != null)
        {
            player = Controller_Player._Player.gameObject;
            direction = -(this.transform.localPosition - player.transform.localPosition).normalized;
        }
        rb = GetComponent<Rigidbody>();
    }

    
    public override void Update()
    {
        // agrego la fuerza de disparo segun la direccion y ejecuto el codigo Update de la clase padre
        rb.AddForce(direction*enemyProjectileSpeed);
        base.Update();
    }

    internal virtual void OnTriggerEnter(Collider other)
    {
        // este proyectil se destruye al entrar en el rango del powerup shockwave
        if (other.gameObject.CompareTag("Explosion"))
        {
            Destroy(this.gameObject);
        }
    }
}
