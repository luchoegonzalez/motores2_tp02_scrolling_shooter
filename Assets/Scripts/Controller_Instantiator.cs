﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// instanciador de enemigos
public class Controller_Instantiator : MonoBehaviour
{
    public float timer=7;
    public  List<GameObject> enemies;
    public GameObject instantiatePos;

    private float time = 0;
    private int multiplier = 20;

    // en cada frame spawneo enemigos si no hay una nave gigante enemiga spawneada
    void Update()
    {
        if (!GiantEnemy.enemigoGiganteEnPantalla)
        {
            timer -= Time.deltaTime;
            SpawnEnemies();
            ChangeVelocity();
        }
    }

    // voy acelerando la velocidad de spawneo (no se esta utilizando este codigo)
    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        if (time > multiplier)
        {
            multiplier *= 2;
            //Increase velocity
        }
    }

    // cada 7 segundos spawneo enemigos al azar de una lista y permito setear cuantos de estos apareceran al mismo tiempo
    private void SpawnEnemies()
    {
        if (timer <= 0)
        {
            float offsetX = instantiatePos.transform.position.x;
            int rnd = UnityEngine.Random.Range(0, enemies.Count);
            for (int i = 0; i < enemies[rnd].GetComponent<Controller_Enemy>().cantidadDeEnemigos; i++)
            {
                offsetX = offsetX + 4;
                Vector3 transform = new Vector3(offsetX, instantiatePos.transform.position.y, instantiatePos.transform.position.z);
                Instantiate(enemies[rnd], transform,Quaternion.identity);
            }
            timer = 7;
        }
    }
}
