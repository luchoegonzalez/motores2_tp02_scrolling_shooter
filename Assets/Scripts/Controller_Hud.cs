﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public Text gameOverText;
    public static bool gameOver;
    public static int points;
    public Text pointsText;
    public Text powerUpText;

    private Controller_Player player;

    void Start()
    {
        gameOver = false;
        gameOverText.gameObject.SetActive(false);
        points = 0;
        player = GameObject.Find("Player").GetComponent<Controller_Player>();
    }

    void Update()
    {
        // al perder y actiarse la variable gameOver se frena el tiempo y aparece el texto Game Over
        if (gameOver)
        {
            Time.timeScale = 0;
            gameOverText.text = "Game Over" ;
            gameOverText.gameObject.SetActive(true);

            // PARA ARREGLAR EL RESTART vuelvo a setear gameOver como false para que no se ejecute mas de una vez el codigo dentro del if
            gameOver = false;
        }

        // segun el contador de powerup mostramos el texto en pantalla para mostrar a cual corresponde
        if (player!=null)
        {
            if (player.powerUpCount <= 0)
            {
                powerUpText.text = "PowerUp: None";
            }
            else if (player.powerUpCount == 1)
            {
                powerUpText.text = "PowerUp: Speed Up";
            }
            else if (player.powerUpCount == 2)
            {
                powerUpText.text = "PowerUp: Missile";
            }
            else if (player.powerUpCount == 3)
            {
                powerUpText.text = "PowerUp: Double shoot";
            }
            else if (player.powerUpCount == 4)
            {
                powerUpText.text = "PowerUp: Laser";
            }
            else if (player.powerUpCount == 5)
            {
                powerUpText.text = "PowerUp: Option";
            }
            else if (player.powerUpCount == 6)
            {
                powerUpText.text = "PowerUp: Shield";
            }
            else if (player.powerUpCount >= 7)
            {
                powerUpText.text = "PowerUp: Shock wave";
            }
        }
        //mostramos el puntaje en pantalla
        pointsText.text = "Score: " + points.ToString();
    }
}
