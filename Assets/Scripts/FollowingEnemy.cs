﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// este enemigo se mueve directamente hacia donde esta el jugador, y hereda el comportamiento de Controller_Enemy
public class FollowingEnemy : Controller_Enemy
{
    private GameObject player;

    private Rigidbody rb;

    private Vector3 direction;

    internal override void Start()
    {
        if (Controller_Player._Player != null)
        {
            player = Controller_Player._Player.gameObject;
        }
        else
        {
            player = GameObject.Find("Player");
        }
        rb = GetComponent<Rigidbody>();
        base.Start();
    }

    public override void Update()
    {
        if (player != null)
        {
            direction = -(this.transform.localPosition - player.transform.localPosition).normalized;
        }
        base.Update();
    }

    void FixedUpdate()
    {
        if (player != null)
            rb.AddForce(direction * enemySpeed);
    }
}
