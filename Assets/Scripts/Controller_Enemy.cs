﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// clase padre de todos los enemigos
public class Controller_Enemy : MonoBehaviour
{
    public float enemySpeed;
    public float xLimit;
    public int cantidadDeEnemigos;

    internal float shootingCooldown;

    public GameObject enemyProjectile;
    public GameObject powerUp;

    internal virtual void Start()
    {
        // genero un cooldown random para que el enemigo dispare
        shootingCooldown = UnityEngine.Random.Range(1, 10);
    }

    public virtual void Update()
    {
        // voy restando el cooldown a cada segundo para que luego dispare
        shootingCooldown -= Time.deltaTime;
        CheckLimits();
        ShootPlayer(new List<Transform> { transform }, 10);
    }

    internal void ShootPlayer(List<Transform> listaDeInstanciadoresDeDisparo, float esperaMaximaDeDisparoAlAzar)
    {
        // cuando el cooldown llega a 0 el enemigo dispara segun una lista que indica desde donde va a realizar disparos
        // y luego vuelve a generar otro cooldown al azar con un nuevo limite maximo
        if (Controller_Player._Player != null)
        {
            if (shootingCooldown <= 0)
            {
                foreach(Transform instanciador in listaDeInstanciadoresDeDisparo)
                {
                    Instantiate(enemyProjectile, instanciador.position, Quaternion.identity);
                }
                shootingCooldown = UnityEngine.Random.Range(1, esperaMaximaDeDisparoAlAzar);
            }
        }
    }

    // si el enemigo se aleja demasiado de la pantalla se destruye
    private void CheckLimits()
    {
        if (this.transform.position.x < xLimit)
        {
            Destroy(this.gameObject);
        }
    }

    // al colisionar con una bala comun, laser o el powerup shockwave el enemigo muere, se suman puntos
    // y se intenta generar un cilindro powerup
    internal virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            GeneratePowerUp();
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Controller_Hud.points++;
        }
        if (collision.gameObject.CompareTag("Laser"))
        {
            GeneratePowerUp();
            Destroy(this.gameObject);
            Controller_Hud.points++;
        }
    }
    internal virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Explosion")) 
        {
            Destroy(this.gameObject);
            GeneratePowerUp();
            Controller_Hud.points++;
        }
    }

    // Genero una probabilidad de que un cilindro powerup aparezca
    internal void GeneratePowerUp()
    {
        int rnd = UnityEngine.Random.Range(0, 3);
        if (rnd == 2)
        {
            Instantiate(powerUp, transform.position, Quaternion.identity);
        }
    }
}
