﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Player : MonoBehaviour
{
    public float speed = 5;

    private Rigidbody rb;

    public GameObject projectile;
    public GameObject doubleProjectile;
    public GameObject missileProjectile;
    public GameObject laserProjectile;
    public GameObject option;
    public GameObject ondaExpansiva;
    public int powerUpCount=0;
    public float duracionDeExplosion;

    // variables de powerups
    internal bool doubleShoot;
    internal bool missiles;
    internal float missileCount;
    internal float shootingCount=0;
    internal bool forceField;
    internal bool laserOn;
    internal float time;
    internal bool explosionIniciada;
    internal bool speedup;

    public static bool lastKeyUp;

    // variables de disparo
    public delegate void Shooting();
    public event Shooting OnShooting;
    public GameObject instanciadorDeBalas;

    private GameObject forceFieldObject;

    internal GameObject laser;

    private List<Controller_Option> options;
    
    // static de este mismo script que permite ser llamado desde cualquier otro
    public static Controller_Player _Player;
    
    // evito que el jugador se destruya al cambiar de escena. Si no hay jugador instancio uno nuevo
    private void Awake()
    {
        if (_Player == null)
        {
            _Player = GameObject.FindObjectOfType<Controller_Player>();
            if (_Player == null)
            {
                GameObject container = new GameObject("Player");
                _Player = container.AddComponent<Controller_Player>();
            }

            DontDestroyOnLoad(_Player);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        forceFieldObject = transform.GetChild(5).gameObject;
        forceFieldObject.SetActive(false);
        reiniciarPowerUps();
    }

    // reinicio valores de powerups
    public void reiniciarPowerUps()
    {
        speed = 17;
        powerUpCount = 0;
        doubleShoot = false;
        missiles = false;
        laserOn = false;
        forceField = false;
        explosionIniciada = false;
        speedup = false;
        options = new List<Controller_Option>();
    }

    private void Update()
    {
        CheckForceField();
        ActionInput();
        Explosion();
    }

    // chequeo si se debe activar el escudo o no
    private void CheckForceField()
    {
        if (forceField)
        {
            forceFieldObject.SetActive(true);
        }
        else
        {
            forceFieldObject.SetActive(false);
        }
    }

    public virtual void FixedUpdate()
    {
        Movement();
    }

    // al apretar la O disparo los distintos tipos de proyectiles segun cual powerup tengamos activo
    public virtual void ActionInput()
    {
        missileCount -= Time.deltaTime;
        shootingCount -= Time.deltaTime;
        if (Input.GetKey(KeyCode.O) && shootingCount<0)
        {
            if (OnShooting!=null)
            {
                OnShooting();
            }

            if (laserOn)
            {
                laser = Instantiate(laserProjectile, instanciadorDeBalas.transform.position, Quaternion.identity);
                laser.GetComponent<Controller_Laser>().parent = instanciadorDeBalas;
            }
            else
            {
                Instantiate(projectile, instanciadorDeBalas.transform.position, Quaternion.identity);
                if (doubleShoot)
                {
                    doubleProjectile.GetComponent<Controller_Projectile_Double>().directionUp = lastKeyUp;
                    Instantiate(doubleProjectile, instanciadorDeBalas.transform.position, Quaternion.identity);
                }
                if (missiles)
                {
                    if (missileCount < 0)
                    {
                        Instantiate(missileProjectile, instanciadorDeBalas.transform.position, Quaternion.Euler(0, 0, 90));
                        missileCount = 2;
                    }
                }
            }
            if (laser != null)
            {
                laser.GetComponent<Controller_Laser>().relase = false;
            }
            shootingCount = 0.1f;
        }
        else
        {
            if (laser != null)
            {
                laser.GetComponent<Controller_Laser>().relase = true;
                laser = null;
            }
        }

        // al apretar la P activo un powerup dependiendo de la cantidad de estos que tenga
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (powerUpCount == 1)
            {
                if (!speedup)
                {
                    speed *= 1.5f;
                    powerUpCount = 0;
                    speedup = true;
                }
            }
            else if(powerUpCount == 2)
            {
                if (!missiles)
                {
                    missiles = true;
                    powerUpCount = 0;
                }
            }
            else if (powerUpCount == 3)
            {
                if (!doubleShoot)
                {
                    doubleShoot = true;
                    powerUpCount = 0;
                }
            }
            else if (powerUpCount == 4)
            {
                if (!laserOn)
                {
                    laserOn = true;
                    powerUpCount = 0;
                }
            }
            else if (powerUpCount == 5)
            {
                OptionListing();
            }
            else if (powerUpCount == 6)
            {
                forceField = true;
                powerUpCount = 0;
            }
            else if (powerUpCount >= 7)
            {
                powerUpCount = 0;
                time = 0;
                explosionIniciada = true;
            }
        }
    }

    // si se activa el powerup Shockwave le hago una animacion de escala al prefab para que colisione con los enemigos
    private void Explosion()
    {
        if (explosionIniciada == true)
        {
            ondaExpansiva.SetActive(true);
            time += Time.deltaTime;

            if (time < duracionDeExplosion)
            {
                float t = time / duracionDeExplosion;
                float escala = Mathf.SmoothStep(3, 70, t);
                ondaExpansiva.transform.localScale = new Vector3(3f, escala, escala);
            }
            else
            {
                ondaExpansiva.SetActive(false);
                explosionIniciada = false;
            }
        }
    }

    // agrego hasta 4 drones que acompañan al juagdor en 4 posiciones distintas y tambien disparan
    private void OptionListing()
    {
        GameObject op=null;
        if (options.Count == 0)
        {
            op = Instantiate(option, new Vector3(transform.position.x-1.5f, transform.position.y-2.5f, transform.position.z), Quaternion.identity);
            options.Add(op.GetComponent<Controller_Option>());
            powerUpCount = 0;
        }
        else if(options.Count == 1)
        {
            op = Instantiate(option, new Vector3(transform.position.x - 1.5f, transform.position.y + 2.5f, transform.position.z), Quaternion.identity);
            options.Add(op.GetComponent<Controller_Option>());
            powerUpCount = 0;
        }
        else if(options.Count == 2)
        {
            op = Instantiate(option, new Vector3(transform.position.x - 2f, transform.position.y - 5, transform.position.z), Quaternion.identity);
            options.Add(op.GetComponent<Controller_Option>());
            powerUpCount = 0;
        }
        else if (options.Count == 3)
        {
            op = Instantiate(option, new Vector3(transform.position.x - 2f, transform.position.y + 5, transform.position.z), Quaternion.identity);
            options.Add(op.GetComponent<Controller_Option>());
            powerUpCount = 0;
        }
    }

    // movimiento del jugador segun el Axis, ademas seteo la ultima key apretada entre la W y la S para determinar para que lado se disparan los proyectiles double
    private void Movement()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(speed* inputX,speed * inputY);
        rb.velocity = movement;
        if (Input.GetKey(KeyCode.W))
        {
            lastKeyUp = true;
        }else
        if (Input.GetKey(KeyCode.S))
        {
            lastKeyUp = false;
        }
    }

    // si colisiono con un enemigo o su proyectil activo la variable gameOver y dejo de mostrar al jugador en patalla
    public virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") || collision.gameObject.CompareTag("EnemyProjectile"))
        {
            // si tengo escudo solo pierdo este powerup
            if (forceField)
            {
                Destroy(collision.gameObject);
                forceField = false;
            }
            else
            {
                gameObject.SetActive(false);
                Controller_Hud.gameOver = true;
            }
        }

        // si colisiono con un powerup lo destruyo y sumo 1 en el contador de estos
        if (collision.gameObject.CompareTag("PowerUp"))
        {
            Destroy(collision.gameObject);
            powerUpCount++;
        }
    }
}
